import java.util.*;

public class TextReverse {

//    public String reversString(String text) {
//        String revers = new StringBuffer(text).reverse().toString();
//        System.out.println(revers);
//        return revers;
//    }

    public String reversStringTest(String str) {
        String[] text = str.split("\\s");
        StringBuilder result = new StringBuilder();
        for (int i = text.length - 1; i >= 0; i--) {
            result.append(text[i]).append(" ");
        }
        result = new StringBuilder(result.toString().trim());
        System.out.println(result);
        return result.toString();
    }

    public String reversText(String str) {
        String[] textArr = str.split("\\s");
        StringBuilder reversString = new StringBuilder();
        for (int i = textArr.length - 1; i >= 0; i--) {
            reversString.append(textArr[i]).append(" ");
        }
        reversString = new StringBuilder(reversString.toString().trim());
        System.out.println(reversString);
        return String.valueOf(reversString);
    }

    public String reversTextTwo(String str) {
        String[] textArr = str.split("\\s");
        StringBuilder result = new StringBuilder();
        for (int i = 0; i < textArr.length / 2; i++) {
            String reversText;
            reversText = textArr[i];
            textArr[i] = textArr[textArr.length - 1 - i];
            textArr[textArr.length - 1 - i] = reversText;
        }
        for (String word : textArr) {
            result.append(word).append(" ");
        }
        result = new StringBuilder(result.toString().trim());
        System.out.println(result);
        return result.toString();
    }

    public String reversTextTest(String str) {
        List<String> stringList = Arrays.asList(str.split("\\s"));
        Collections.reverse(stringList);
        StringBuilder result = new StringBuilder();
        for (String word : stringList) {
            result.append(word).append(" ");
        }
        result = new StringBuilder(result.toString().trim());
        System.out.println(result);
        return result.toString();
    }

    public String reversStringTestTwo(String str) {
        StringBuilder reversString = new StringBuilder();
        List<String> list =Arrays.asList(str.split("\\s"));
        Collections.reverse(list);
        list.forEach((word)-> reversString.append(word).append(" "));
        System.out.println(reversString.toString().trim());
        return reversString.toString().trim();
    }

    public static void main(String[] args) {
        TextReverse textReverse = new TextReverse();
        String text = "В бытность мою в С—м уезде мне часто приходилось бывать " +
                "на Дубовских огородах у огородника Саввы Стукача, или попросту Савки";
//		textReverse.reversString("Do or do not, there is no try");
        textReverse.reversStringTest(text);
        System.out.println("------------");
		textReverse.reversText(text);
        System.out.println("------------");
        textReverse.reversStringTest(text);
        System.out.println("------------");
        textReverse.reversTextTwo(text);
        System.out.println("------------");
        textReverse.reversStringTestTwo(text);
        System.out.println("-----------------");
        textReverse.reversTextTest("text");
    }


}
