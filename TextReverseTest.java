import org.junit.Assert;
import org.junit.Rule;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

class TextReverseTest {
	String testText = "В бытность мою в С—м уезде мне часто приходилось бывать " +
			"на Дубовских огородах у огородника Саввы Стукача, или попросту Савки";
	String actual = "Савки попросту или Стукача, Саввы огородника у огородах Дубовских " +
			"на бывать приходилось часто мне уезде С—м в мою бытность В";
	String actualFail = "Савки попросту или Стукача, Саввы огородника у огородах Дубовских " +
			"на бывать приходилось часто мне уезде С—м в мою бытность S ";
	private TextReverse textReverse;

	@BeforeEach
	private void initTextReverse() {
		textReverse = new TextReverse();
	}
	@Test
	void reversStringTest() {
		Assertions.assertEquals(actual, textReverse.reversStringTest(testText));
	}
	@Test
	void reversStringTestNegative() {

		Assertions.assertNotEquals(actualFail, textReverse.reversStringTest(testText));
	}
	@Test
	void reversText() {
		Assertions.assertEquals(actual, textReverse.reversText(testText));
	}
	@Test
	void reversTextTwo() {
		Assertions.assertEquals(actual, textReverse.reversTextTwo(testText));
	}
	@Test
	void reversTextTest() {
		Assertions.assertEquals(actual, textReverse.reversTextTest(testText));
	}
	@Test
	void reversStringTestTwo() {
		Assertions.assertEquals(actual, textReverse.reversStringTestTwo(testText));
	}

}